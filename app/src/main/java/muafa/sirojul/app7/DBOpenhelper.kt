package muafa.sirojul.app7

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenhelper(context : Context) : SQLiteOpenHelper (context,DB_Name,null,DB_Ver){
    override fun onCreate(db: SQLiteDatabase?) {
        val tMhs = "create table mhs(nim text primary key, nama text not null, prodi text not null)"
        db?.execSQL(tMhs)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }

    companion object{
        val DB_Name = "mahasiswa"
        val DB_Ver = 2
    }

}